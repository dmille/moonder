#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "moonder.h"
#include <string.h>
#include <signal.h>

long private_msqid;
char name[NAME_MAX];

void chat(long,int);
void printSelection(char);
char getSelection(char);
void printStatus(char);
void signal_callback_handler(int signum) {
  printf("\nGood-bye from Moonder\n");

  //send Quit message to server
  struct resp_buf rbuf = {2,'Q'};
  if(msgsnd(private_msqid, &rbuf, sizeof(char), 0) == -1)
    perror("msgsnd");

  // clean up
  msgctl(private_msqid, IPC_RMID, NULL);
  // Terminate
  exit(signum);
}

int main(int argc, char *argv[]) {
  signal(SIGINT, signal_callback_handler);
  char info[INFO_MAX];

  long public_msqid;
  FILE * fin;

  // join buffer
  struct join_buf jbuf;

  // candidate buffer from server
  struct cand_buf cbuf;

  // response buffer to server
  struct resp_buf rbuf;

  // buffer to retrive candidate msqid from server
  struct msqid_buf mbuf;

  // buffer to receive messages from other clients
  struct chat_buf rcv;

  int len;
  char select = ' ';

  if(argc != 2) {
    printf("Usage: %s <profile file>\n", argv[0]);
    exit(1);
  }
  
  // read profile file
  fin = fopen(argv[1],"rt");
  fgets(name,NAME_MAX,fin);
  fgets(info,INFO_MAX,fin);
  fclose(fin);

  // do some string stuff to make them usable for the application
  strcpy(jbuf.profile.name,name);
  strcpy(jbuf.profile.info,info);

  len = strlen(jbuf.profile.name);
  if (name[len-1] == '\n') name[len-1] = '\0';
  len = strlen(jbuf.profile.info);
  if (info[len-1] == '\n') info[len-1] = '\0';
  
  strcpy(jbuf.profile.name,name);
  strcpy(jbuf.profile.info,info);

  // join PUBLIC MQ
  if ((public_msqid = msgget(JOIN_KEY, 0644)) == -1) {
    perror("msgget");
    exit(1);
  }

  // start PRIVATE MQ
  if ((private_msqid = msgget(getpid(),0644 | IPC_CREAT)) == -1) {
    perror("msgget");
    exit(1);
  }
    
  jbuf.msqid = private_msqid;

  // set m_type of responses to 2
  rbuf.m_type = 2;

  // send join request with details (buf)
  if (msgsnd(public_msqid, &jbuf, sizeof(struct user_profile), 0) == -1)
    perror("msgsnd");
  
  // wait for acknowledgement message (only m_type 3)
  if (msgrcv(private_msqid, &jbuf, sizeof(struct user_profile),3,0) == -1)
    perror("msgrcv");
  // start candidate loop
  printf("Welcome to Moonder, %s. Perhaps you want to know:\n", name);
  // listen for incoming chat requests
  while(select != 'Q') {

    if(msgrcv(private_msqid, &rcv, sizeof(struct chat_message),5,IPC_NOWAIT)>0) {
      printf("\n-------------------------------------------------------------\n\n");
      printf("CHAT FROM %s\n", rcv.data.name);
      printf("%s: %s", rcv.data.name, rcv.data.message);
      chat(rcv.data.msqid,1);
    }
    // Get candidate from server and print details (only m_type 1)
    if(msgrcv(private_msqid, &cbuf, sizeof(struct cand_profile),1,0) == -1)
      perror("msgrcv");
    printf("\n-------------------------------------------------------------\n\n");
    printf("%s\n",cbuf.data.profile.name);
    printf("%s\n",cbuf.data.profile.info);
    printStatus(cbuf.data.status);
    // get selection and put in rbuf to send to server
    rbuf.sel = getSelection(cbuf.data.status);
    if(rbuf.sel == 'Q')
      signal_callback_handler(2)
;
    // send response to server
    if(msgsnd(private_msqid, &rbuf, sizeof(char), 0) == -1)
      perror("msgsnd");
    
    // if client chose to chat, receive candidate msqid and begin chat
    if(rbuf.sel == 'C') {
      // get msqid of cand from server
      if(msgrcv(private_msqid, &mbuf, sizeof(long),4,0)==-1)
	perror("msgrcv");

      if(mbuf.msqid != -1)
	// chat with candidate
	chat(mbuf.msqid,0);
      else
	// if user is not accessible
	printf("User is no longer online\n"); 
      
      // let server know you're done chatting
      if(msgsnd(private_msqid, &rbuf, sizeof(char), 0) == -1)
	perror("msgsnd");
     
    }
  }
}

void chat(long target_msqid, int stflag) {
  struct chat_buf snd;
  struct chat_buf rcv;
  int exitflag = 0;
  strcpy(snd.data.name,name);
  snd.data.msqid = private_msqid;
  snd.m_type = 5;
  // if user is initiater of chat print line
  if(stflag == 0)
    printf("\n--------------------------------------------------------------\n\n");

  // as long as neither user has quit, continue
  while(exitflag != 1){
    
    printf("You: ");
    fgets(snd.data.message,MAX_INPUT,stdin);

    // if user wants to quit and enters Q, change exit flag
    if(strlen(snd.data.message) == 2 && snd.data.message[0] == 'Q')
      exitflag = 1;

    if(msgsnd(target_msqid, &snd, sizeof(struct chat_message), 0) == -1)
	perror("msgsnd");

    // if exit flag has changed break after sending message to other user
    if(exitflag == 1) break;
    
    // receive message from other user
    if(msgrcv(private_msqid, &rcv, sizeof(struct chat_message), 5, 0) == -1)
	perror("msgrcv");
    
    // if other user wants to quit entered Q, display message and change exit flag
    if(strlen(rcv.data.message) == 2 && rcv.data.message[0] == 'Q'){
	exitflag = 1;
	printf("%s ENDED CHAT\n", rcv.data.name);
    }
    else
      printf("%s: %s", rcv.data.name, rcv.data.message);
    
  }
  
}

// prints appropriate status according to status
void printStatus(char s){
  if(s == 'L')
    printf("LIKED\n");
  else if(s == 'M')
    printf("MATCHED\n");
  return;
}

// returns user input as a char for choosing what operation to perform
char getSelection(char s){
  char ibuf[MAX_INPUT];
  while(1){
    printSelection(s);
    fgets(ibuf,MAX_INPUT,stdin);
    if((int)strlen(ibuf) == 2){
      if((char)ibuf[0] == 'P' || (char)ibuf[0] == 'Q')
	return (char)ibuf[0];
      if(s == 'M' && (char)ibuf[0] == 'C')
	return (char)ibuf[0];
      if(s == 'N' && (char)ibuf[0] == 'L')
	return (char)ibuf[0];  
    }
    printf("Invalid Input\n");
  }
}

void printSelection(char s) {
  if(s == 'L')
    printf("Enter Selection (P,Q): ");
  else if(s == 'M')
    printf("Enter Selection (P,C,Q): ");
  else
    printf("Enter Selection (P,L,Q): ");
}
