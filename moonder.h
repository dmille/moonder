#define JOIN_KEY 1139089549
#define NAME_MAX 256
#define INFO_MAX 256
#define MAX_INPUT 256
#define MAX_USERS 20 // disregard
#include <pthread.h>

// basic user profile containing only name and info
struct user_profile {
  char name[NAME_MAX];
  char info[INFO_MAX];
};

// profile to send to client through cand_buf
struct cand_profile {
  struct user_profile profile;
  char status;
};

struct chat_message {
  char message[MAX_INPUT];
  char name[NAME_MAX];
  long msqid;
};

// candidate buffer for server to send to client
struct cand_buf {
  long m_type;
  struct cand_profile data;
};

// chat buffer for communicating with other clients
struct chat_buf {
  long m_type;
  struct chat_message data;
};

// used to send msqid of candidate to client when chat has been initialized
struct msqid_buf {
  long m_type;
  long msqid;
};

// join buffer for client to join server and for server to send acknowledge message
struct join_buf {
  long msqid;
  struct user_profile profile;
};

// reponse buffer for reply from client to server
struct resp_buf {
  long m_type;
  char sel;
};

// node used for a users profile
typedef struct profile_node {
  struct user_profile profile;
  pthread_t thread;
  struct profile_node *next;
  long msqid;
  struct liked_node *likes;
}node;

typedef struct liked_node {
  struct liked_node *nextl;
  long msqid;
}liked;


// adds a profile to the circular linked list
struct profile_node * add_profile(struct profile_node *pointer, struct join_buf * buf) {
  node *head = pointer;

  while(pointer->next != head)
    pointer = pointer->next;

  pointer->next = (node *) malloc (sizeof(node));

  pointer = pointer->next;
  pointer->profile = buf->profile;
  pointer->msqid = buf->msqid;
  pointer->next = head;

  pointer->likes = (liked *) malloc (sizeof(liked));
  pointer->likes->nextl = NULL;

  return pointer;
}

// checks to see if profile with msqid is on the system or not
int check_profile(struct profile_node *pointer, long msqid) {
  node *head = pointer;
  while((pointer->next)->msqid != msqid && pointer->next!= head)
    pointer = pointer->next;
  if(pointer->next == head) return 0;
  else return 1;
}

// returns number of users on the system to server (for debugging)
int user_count(struct profile_node *pointer) {
  node *head = pointer;
  int count = 0;
  while(pointer->next != head){
    pointer = pointer->next;
    count++;
  }
  return count;
}

// deletes a profile from the circular linked list
int delete_profile(struct profile_node *pointer, long msqid) {
  node *head = pointer;
  while((pointer->next)->msqid != msqid && pointer->next != head)
    pointer = pointer->next;
  if(pointer->next==head){
    printf("Profile with msqid of %ld does not exist\n", msqid);
    return 1;
  }
  node *temp;
  temp = pointer->next;
  pointer->next = temp->next;
  free(temp);
  return 0;
}

// prints all profiles in the linked list (for testing)
void print_all_profiles(struct profile_node *pointer) {
  node *head = pointer;

  while(pointer->next != head) {
    pointer = pointer->next;
    printf("%s - %s\n", pointer->profile.name, pointer->profile.info);
  }

}

// initialized the linked list
struct profile_node * init_profile_list() {
  node * head;
  head = (node *)malloc(sizeof(node));
  head->next = head;
  return head;
}
