# README #

### Final Assignment for CPS590: Introduction to Operating Systems. Grade: A+ ###

The focus of the assignment was working with threads and IPC using C in linux as well as mutual exclusion using semaphores.

The Assignment itself is based off of backend functionality of the app Tinder where the server handles all communication by users connected. The description of the assignment is included under assign2.txt

Although groups of up to 3 were allowed, I implemented the assignment on my own.

## TO COMPILE ##
MoonderServer:
```
#!bash
gcc MoonderServer.c -o MoonderServer -lpthread
```
Moonder Client:
```
#!bash
gcc Moonder.c -o MoonderClient -lpthread
```

## Running the application ##
Moonder Server needs to be running in order to start the client program. Client programs need a profile (also included as data*) to run
```
#!bash
./MoonderServer

./MoonderClient data1

./MoonderClient data2
```