#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "moonder.h"
#include <pthread.h>
#include <assert.h>
#include <signal.h>

node * head;

void *user_thread(void *);
char getStatus(node *, node *);
void likeCandidate(node *, long) ;

long PUB_msqid;

// Define the function to be called when ctrl-c (SIGINT) signal is sent to process
void signal_callback_handler(int signum) {
   printf("\nGood-bye from Moonder\n");
   // Cleanup and close up stuff here
   msgctl(PUB_msqid, IPC_RMID, NULL);
   // Terminate
   exit(signum);
}

int main(void) {
  signal(SIGINT, signal_callback_handler);
  long msqid;
  // join buffer from client
  struct join_buf jbuf;
  int rc;
  node * profile;

  head = init_profile_list();

  if ((msqid = msgget(JOIN_KEY, 0644 | IPC_CREAT)) == -1) {
    perror("msgget");
    exit(1);
  }
  PUB_msqid = msqid;
  // start JOIN MQ listener loop
  for(;;) {
    if (msgrcv(msqid, &jbuf, sizeof(struct user_profile), 0, 0) == -1) {
      perror("msgrcv");
      exit(1);
    }
    // add new profile to linked list
    profile = add_profile(head,&jbuf);

    // start new thread for new user
    rc = pthread_create(&profile->thread, NULL, &user_thread, profile);
    assert(0 == rc);
  }
}

void *user_thread( void * p ) {
  node * current;
  node * self = (node *) p;
  // ackgnowledgement buffer to client
  struct join_buf abuf;

  // candidate buffer to client
  struct cand_buf cbuf;

  // response buffer from client
  struct resp_buf rbuf;

  // msqid buffer for sending candidate id to client for chat
  struct msqid_buf mbuf;

  long msqid = self->msqid;
  current = head -> next;
  
  // set msqid(m_type) of acknowledgement messages to 3
  abuf.msqid = 3;
  
  // send user acknowedgement message on private MQ
  if(msgsnd(msqid,&abuf,sizeof(struct user_profile), 0) == -1)
    perror("msgsnd");

  // set m_type of candidates to 1
  cbuf.m_type = 1;
  
  // start listen response loop
  for(;;) {
    // cycle through CLL of profiles until not own and not header gets selected
    while(current == head || current == self)
      current = current->next;
    // set profile of cbuf data to the profile of current candidate
    cbuf.data.profile = current->profile;
    // set status of cbuf to status between you and candidate
    cbuf.data.status = getStatus(self,current);
    printf("\n%s\n",cbuf.data.profile.name);
    // send candidate to client
    if(msgsnd(msqid,&cbuf,sizeof(struct cand_profile), 0) == -1)
      perror("msgsnd");
    // receive response from client (only m_type 2)
    if(msgrcv(msqid,&rbuf,sizeof(char),2,0) == -1)
      perror("msgrcv");

    // If user selects Q delete profile and exit
    if(rbuf.sel == 'Q') {
      delete_profile(head,msqid);
      return 0;
    }
    
    //  If user selects L, like the candidate
    if(rbuf.sel == 'L') {
      likeCandidate(self,current->msqid);
    }

    //  If user decides to chat, send user msqid of candidate
    if(rbuf.sel == 'C') {
      if(check_profile(head,current->msqid) == 0)
	mbuf.msqid = -1;		
      else 
	mbuf.msqid = current->msqid;

      mbuf.m_type = 4;
      if(msgsnd(msqid,&mbuf,sizeof(long),0) == -1)
	perror("msgsnd");

      // wait for user to signal chatting is done
      if(msgrcv(msqid, &rbuf, sizeof(char),2,0) == -1)
	  perror("msgrcv");
    }
   
    current = current->next;
  }

}

// returns status N if neither liked or matched
// returns status L if liked but not matched
// returns status M if matched
char getStatus(node * me, node * candidate){
  long ca_msqid = candidate->msqid;
  long my_msqid = me -> msqid;
  liked * current = me -> likes;
  char status = 'N';
  if(current == NULL) return status;
  // while the next node is not NULL
  while(current != NULL) {
    // if the current node has the same msqid as candidates...
    if(current->msqid == ca_msqid){
      // start searching candidates nodes for match with own msqid
      current = candidate->likes;
      if(current == NULL) return('L');
      while(current != NULL) {
	// if own is found in candidates msqid then you are a match
	if(current->msqid == my_msqid)
	  return ('M');
	current = current->nextl;
      }
      // if not found, then you still like them
      return('L');
    }
    current = current->nextl;  
  }
  // return status (N) for neither since you don't like them and you aren't matched
  return status;
}

void likeCandidate(node * me, long cand_id) {
  liked * current = me->likes;
  
  // navigate to the last node
  if(current != NULL)
    while(current -> nextl != NULL)
      current = current->nextl;

  // create a new node after the last node
  current->nextl = (liked *) malloc (sizeof(node));
  current = current->nextl;
  
  // set new nodes next value to NULL and msqid to the id passed by function
  current->nextl = NULL;
  current->msqid = cand_id;

  printf("\nCANDIDATE LIKED\n");

}
