Assignment 2 (Clarifications to this assignment will be in file assign2Addendum.txt)

Due: as per CMF, with maximum extension as per CMF; each 24 hrs past due date is -5%
     to a maximum of maximum extension date/time in CMF.

Prof will go over the assignment in class on Wed March 19 only (if someone reminds her.)
-------------------------------------------------------------------------------------

NOTE: MQ = "System V message queue" in the following.


OBJECTIVE:
  Write an social discovery application, Moonder, that facilitates communication
  between mutually interested Ryerson CS people so they can discuss topics such 
  as courses, assignments, and where to get a good espresso near campus.

  1. Moonder shows you someone it thinks you should know
  2. You can anonymously like this person or skip to the next suggestion
    . . .  
  3. If someone you like happens to like you back, then Moonder lets you chat 
     with that person within the program.

HOW Moonder WORKS:
  Moonder displays "candidates" to the client one-by-one. For each candidate, the client may:
     "pass" (P), 
     "like" (L), 
     "chat" (C),
     "quit Moonder" (Q). 
  If client "likes" a candidate who has also "liked" them, then they are A MATCH. 
  The client may chat with a candidate with whom they match.
  The client may NOT chat with any candidates with whom they have not matched.

  Candidates are displayed (cycled through) in an infinite loop until the client QUITS. 
  Thus, a candidate may be displayed numerous times. You may display in any order, as
  long as the ordering is fixed (candidate X+1 always displays immediately after candidate X).

  Note that P, L, C, Q have following caveats:
  Clients are not allowed the option to chat (C) with a candidate unless that candidate
     is already a match. 
  Clients are not allowed the option to like (L) a candidate whom they already like (there
     is no point, since client ALREADY likes candidate!).
  When client passes on a candidate who they already like, this does not alter the like
     (candidate is still liked--the pass just moves on to view the next candidate.)
  When client passes on a candidate who they already match, this does not alter the match
     (candidate is still matched--the pass just moves on to view the next candidate.)
  The client always has the option to quit Moonder (Q).
  
GUI:
  There is none. You will focus on the "back-end" of Moonder only, and will make a
  TUI (Text User Interface) for testing purposes.
  HOWEVER, make your client code appropriately modular so that someone could easily 
  replace the TUI with a GUI at a future date.

CANDIDATE DISPLAY:
  A candidate is "displayed" by printing, on stdout, the candidate's:
     name 
     info
     status, which is one of:
        -"MATCHED" if this candidate has previously been matched to client; else,
        -"LIKED" if this candidate has previously been liked by client; else,
	-"" if this candidate has not been previously liked or matched by client.
  After the candidate is displayed, a one-line menu is displayed. This menu contains
  at least P (Pass) and Q (Quit). It may contain L (Like), or C (Chat), depending on 
  the candidate's status. 
  
STATE:
  Moonder it largely stateless.
  Each time a client program starts, Moonder assumes this is a new client.
  Moonder discards messages once they received/displayed.
  Nothing is retained after the Moonder server shuts down (i.e., clients are lost).
  The server does not maintain a client after the client quits.
  When a client quits, they should not show up in other client's displays. If some
  reference to a quitted client still exists, do something appropriate and continue. 
  For example, Moonder must do something reasonable (i.e., not crash) in this case:
       Client X is looking at client Y's display, and wants to chat to client Y. 
       However, client Y quits Moonder just before client X hits the "C" key.
  Note that a client may be killed with an un-catchable signal such as "kill -9".
  Your server doesn't have to account for this situation. (You may handle it in a
  reasonable way, but it will not count for extra marks.)

COMMUNICATION:
  Moonder must run, and will be tested only, on a CS moon.
  Moonder consists of one server program. Multiple clients use Moonder by running 
  an instance of the client program. 
  Client/server communication takes place using Linux Message Queues (MQs).
  JOIN MQ: used for client to join Moonder; its key is hardcoded and known to both 
           client and server.
  Client MQs: Each client may communicate with server using any number of private
           MQs you require.
  You may use System V message queues as per lab08, or POSIX message queues.
  (This assignment uses System V terminology, however.)
  As this is not a networking course, communication by sockets is NOT required for 
  this assignment; however, if you insist on using them, you may do so (and alter
  the assignment communication details as appropriate.)

SERVER:
  Monitors the JOIN MQ for new client requests to join Moonder. 
  When a new client wants to join:
    Server does minimal set-up and spawns a new thread to handle the client. 
    The spawned thread sends the client an Acknowledgement message, on this client's
    private MQ (which was sent in the client's join message).
  The server thread communicates with the client using any private MQs you require.
  When the client disconnects, server thread does clean-up and terminates.
  The server is terminated with an appropriate signal, which it should catch to 
  perform clean-up before terminating.

CLIENT:
  Takes one command-line argument, which is the user's profile file.
  May spawn threads, create MQs, and send data to server, as necessary.
  Requests Moonder service by sending server a JOIN message on the JOIN MQ.
  Gets acknowledgement message on its private MQ, does set-up, starts Candidate Loop.

  Candidate Loop:
    Gets a candidate's details from server (name, info, status) using MQ(s).
    Writes this candidate's details and menu on stdout and reads (from stdin) client's 
    response, which is one of P (Pass), L (Like), C (Chat), Q (quit Moonder), where:
       P moves on to display next candidate
       L "likes" candidate and then moves on to display next candidate
       C Initiates a chat with this candidate (temporarily enter Chat)
       Q quits Moonder

  Chat:
    Chat to candidate, using some temporary MQ, which is private between client and 
    this candidate. 
    Chat Loop:
      Write chat-message to this candidate. 
      Display candidate's response.
    Chat Loop ends when client or candidate types Q (or for some other good reason.)
    Upon loop end, clean up (including deleting private MQ).
    After chat ends, return to Candidate Loop (continue with next candidate.)

  Quit:
  When client wants to quit Moonder, it sends QUIT message to server, cleans-up, and 
  terminates.


MQ MESSAGES:
  Messages sent on your MQs will each have an integer type (field mtype for System V MQs).
  You could use the type field to distinguish among different categories of messages, if
  necessary.
  If information is too great to fit into one message, you may divide it over more than
  one message, as necessary.
  You may (should) impose a limit on the information you allow, to make it easier 
  to send in a message. For example, you might restrict the size/characters of 
  chat-messages and user profile information. 


PROFILE FILES:
  When a client joins Moonder, s/he must provide her profile file as a command-line
  argument to the client program. The file should contain exactly 2 lines--this client's
    -name, and
    -information
  You may place reasonable restrictions on the length of this data.
  Moonder uses this information when displaying this client as a candidate.
  Clients may happen to have the same name (and info). Handle this appropriately.
  IT IS IMPORTANT your profile files have the correct 2-line format. Otherwise
  we cannot test your programs, and you will get zero marks for "program execution".


THREADS:
  Your clients may spawn threads, if necessary.
  Your server must spawn at least one thread per client. Other threads may be
  spawned as necessary.


WHO MOONDER THINKS YOU SHOULD KNOW:
  In "OBJECTIVE" above, it stated:
     1. Moonder shows you someone it thinks you should know
  FOR YOUR ASSIGNMENT, MOONDER THINKS YOU SHOULD KNOW EVERY OTHER CLIENT CURRENTLY
  ON MOONDER.

  Below, is how "Full Moonder" could decide who you should know. You are 
  NOT required to implement Full Moonder for this assignment, but you must use 
  appropriate design, modularity and data-structures so that your assignment could 
  be easily extended to Full Moonder later. 

  Here is how Full Moonder would decide who you should know:
   1. Based on how near they are to you. 
      Client X's candidates include only those clients currently within a 
      R-kilometer radius of X. R is specified by client X, and is modifiable. 
      Each client spawns a GPS thread which periodically sends a message 
      to the server updating this client's current GPS coordinates (which it 
      obtains using the OS's GPS API).
   2. Based on client "information" which could be updated by the client at
      any time (by choosing "update profile" from some menu). Thus, client X
      would be a candidate for client Y only if Moonder considered them to
      have some common information (e.g., they both like mocha-lattes, they 
      are both taking CPS590, etc.)
   3. HOWEVER, REMEMBER YOU ARE NOT TO IMPLEMENT FULL MOONDER! Your version
      simply assumes all current clients are candidates for client X.

GLOBAL DATA:
  Use globals only for data that must be shared among threads; account for
  race conditions. Do not use global data when local data would suffice. 
  

MAXIMUM CLIENTS:
  Your server may restrict the number of clients to N, if you wish (server
  macro definition).  

SYNCHRONIZATION/MUTUAL EXCLUSION:
  Semaphores are the only method allowed.

CLEAN-UP:
  Your code should clean up appropriately. e.g., delete all MQs, temporary 
  files etc.
  Functions such as atexit and on_exit might be useful. 


ROBUSTNESS/ASSUMPTIONS:
  Programs should be reasonably robust (do reasonable error-checking). i.e.:
  If a program detects an unexpected error, such as a MQ disappearing unexpectedly,
  then the program should clean-up and terminate gracefully.
  If unexpected messages are received, the receiver may simply ignore them.
  Server may assume messages it receives are correctly formatted (by client). 
  Timed message receives are not required for this assignment, but you might
  find them useful, especially for chat.


EXAMPLE:
Below is an example of a user using the Moonder application, assuming a Moonder 
server is running on this moon. Note:
  -when client joins, there are already 4 currently participating clients (candidates).
  -one of the candidates quits during this example.
  - ">" is the user's shell prompt. 
  -client menu response is shown after ": ". 
Your output may differ.


> Moonder DWoit_profile
    Welcome to Moonder, Dr. Woit. Perhaps you want to know: 
    --------------------------------------------------------------------------
    Alireza Sadeghian
    I am am Chair of Comp Sci. I teach CPS213. Cryptography interests me.
    Enter Selection (P,L,Q): L
    --------------------------------------------------------------------------
    Dave Mason
    I love programming languages, computational thinking, and peanut butter.
    Enter Selection (P,L,Q): P
    --------------------------------------------------------------------------
    Abdolreza Abhari
    I am interested in operating systems and databases. Call me Abdy.
    Enter Selection (P,L,Q): P
    --------------------------------------------------------------------------
    Cherie Ding
    I studied in Singapore. I like web analytics and usage mining.
    Enter Selection (P,L,Q): L
    --------------------------------------------------------------------------
    Alireza Sadeghian
    I am am Chair of Comp Sci. I teach CPS213. Cryptography interests me.
    LIKED
    Enter Selection (P,Q): P
    --------------------------------------------------------------------------
    Dave Mason
    I love programming languages, computational thinking, and peanut butter.
    Enter Selection (P,L,Q): P
    --------------------------------------------------------------------------
    Cherie Ding
    I studied in Singapore. I like web analytics and usage mining.
    MATCHED
    Enter Selection (P,C,Q): C
    --------------------------------------------------------------------------
    You: Hi there cherie!
    Cherie Ding: Helloooooo.
    You: I have to go now.
    cherie Ding: OK. See you.
    You: Q
    --------------------------------------------------------------------------
    Alireza Sadeghian
    I am am Chair of Comp Sci. I teach CPS213. Cryptography interests me.
    LIKED
    Enter Selection (P,Q): P
    --------------------------------------------------------------------------
    Dave Mason
    I love programming languages, computational thinking, and peanut butter.
    Enter Selection (P,L,Q): P
    --------------------------------------------------------------------------
    CHAT FROM Alireza Sadeghian
    Alireza Sadeghian: hello prof woit
    You: hello chair sadeghian
    Alireza Sadeghian: what's up?
    You: just testing out Moonder. You?
    Alireza Sadeghian: same here. Bye
    You: bye for now.
    Alireza Sadeghian ENDED CHAT
    --------------------------------------------------------------------------
    Cherie Ding
    I studied in Singapore. I like web analytics and usage mining.
    MATCHED
    Enter Selection (P,C,Q): P
    --------------------------------------------------------------------------
    Alireza Sadeghian
    I am am Chair of Comp Sci. I teach CPS213. Cryptography interests me.
    MATCHED
    Enter Selection (P,C,Q): Q
    --------------------------------------------------------------------------
    Good-bye from Moonder. 
>

GROUPS:
  You may work in groups of 1-3 people.  There will be no marking benefit/penalty.
  Use Assign1 file group.txt as a guideline to see who submits what. 


DELIVERABLES:
   Moonder.c        //the client source program
   Moonder          //the client executable
   MoonderServer.c  //the server executable program
   MoonderServer    //the server executable
   *                //any other files it needs (such as .h files)
   Makefile         //OPTIONAL

